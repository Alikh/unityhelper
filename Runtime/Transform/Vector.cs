﻿using UnityEngine;

namespace Transform
{
    public static class Vector
    {
        public static Vector3 Sum(this Vector3 current, Vector3 other)
            => new Vector3(current.x + other.x, current.y + other.y, current.z + other.z);
    }
}